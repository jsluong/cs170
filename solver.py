#!/usr/bin/env python

from __future__ import division
from queue import PriorityQueue
import argparse
import random
import datetime

"""
===============================================================================
  Please complete the following function.
===============================================================================
"""
class MyPriorityQueue(PriorityQueue):
    def __init__(self):
        PriorityQueue.__init__(self)
        self.counter = 0

    def put(self, item, priority):
        PriorityQueue.put(self, (priority, self.counter, item))
        self.counter += 1

    def get(self, *args, **kwargs):
        _, _, item = PriorityQueue.get(self, *args, **kwargs)
        return item

def generate_constraints(constraints):
  banned = {}

  for single_constraint in constraints:
    for i in single_constraint:
      if i not in banned:
        banned[i] = {}
      for j in single_constraint:
        if i != j:
          banned[i][j] = ""

  return banned

rng = random.SystemRandom()
def schema(items, i, id, banned):
  if id % 2 == 0:
    if (items[i][3] == 0) or (items[i][2] == 0):
      return 0
  if id//2 == 0:
    return items[i][4] / (items[i][3]+0.01)
  if id//2 == 1:
    return items[i][4] / (items[i][3]+items[i][2]+0.01)
  if id//2 == 2:
    return items[i][3]
  if id//2 == 3:
    return items[i][2]
  if id//2 == 4:
    return items[i][3]+items[i][2]
  if id//2 == 5:
    return len(banned[items[i][1]]) if (items[i][1] in banned) else 0
  if id//2 == 6:
    return -items[i][4]
  else:
    return rng.randint(rng.randint(1, 100), rng.randint(100,200))

def solve(P, M, N, C, items, constraints):
  """
  Write your amazing algorithm here.

  item[0] = name
  item[1] = class
  item[2] = weight
  item[3] = cost
  item[4] = resale

  Return: a list of strings, corresponding to item names.
  """
  banned = generate_constraints(constraints)
  used_classes = {}
  ret = []
  max_ret = []
  total = M
  originalM = M
  originalP = P

  d = {}

  read_input_max(args.input_max, d)

  problem = int(str(args.input_file)[7:][:-3])
  max_total = d[problem]
  rng = random.SystemRandom()

  for z in range(0, 100):
    queue = MyPriorityQueue()
    for i in range(N):
      if (items[i][3] >= items[i][4]): 
        pass
      # if (items[i][3] == 0) or (items[i][4] == 0):
      #   queue.put(items[i], 0)

      else:
        priority = schema(items, i, z, banned)
        queue.put(items[i], priority)

    while not queue.empty():
      temp = queue.get()
      if (temp[1] not in used_classes) and (P - temp[2] >= 0) and (M - temp[3] >= 0):
          M -= temp[3]
          P -= temp[2]
          ret.append(temp[0])
          if temp[1] in banned:
            used_classes.update(banned[temp[1]])
          total += (temp[4] - temp[3])

    # print("(" + str(problem) + " " + str(z) + ") " + str(total) + "    " + str(max_total))
    if total > max_total:
      read_input_max(args.input_max, d)
      problem = int(str(args.input_file)[7:][:-3])
      max_total = d[problem]

      if total > max_total:
        print("UPDATE (" + str(problem) + " " + str(z) + "): +" + str(total-max_total) +"!!!")
        max_ret = ret
        max_total = total
        d[problem] = max_total
        write_output_max(args.input_max, d)
        write_output_results(problem, max_ret)
        write_team_name(problem)

    M = originalM
    P = originalP
    total = M
    ret = []
    used_classes = {}

"""
===============================================================================
  No need to change any code below this line.
===============================================================================
"""

def read_input_problem(filename):
  """
  P: float
  M: float
  N: integer
  C: integer
  items: list of tuples
  constraints: list of sets
  """
  with open(filename) as f:
    P = float(f.readline())
    M = float(f.readline())
    N = int(f.readline())
    C = int(f.readline())
    items = []
    constraints = []
    for i in range(N):
      name, cls, weight, cost, val = f.readline().split(";")
      items.append((name, int(cls), float(weight), float(cost), float(val)))
    for i in range(C):
      constraint = set(eval(f.readline()))
      constraints.append(constraint)
  return P, M, N, C, items, constraints

def read_input_max(filename, d):
  with open(filename) as f:
    for line in f:
      (key, val) = line.split()
      d[int(key)] = float(val)

def write_output_max(filename, d):
  with open(filename, "w") as f:
    for key, value in d.items():
      f.write("{0}	{1}\n".format(key, value))

def write_team_name(filename):
  filename = "./output/team_name.txt"
  time = '{:%m%d %H%M}'.format(datetime.datetime.now())
  team_name = "someBODY once told me the world was gonna roll me "
  with open(filename, "w") as f:
    f.write(team_name + time)

def write_output_results(filename, items_chosen):
  filename = "./output/problem" + str(filename) + ".out"
  with open(filename, "w") as f:
    for i in items_chosen:
      f.write("{0}\n".format(i))

if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="PickItems solver.")
  parser.add_argument("input_file", type=str, help="____.in")
  parser.add_argument("input_max", type=str, help="____.max")
  args = parser.parse_args()
  problem = int(str(args.input_file)[7:][:-3])

  P, M, N, C, items, constraints = read_input_problem(args.input_file)
  solve(P, M, N, C, items, constraints)